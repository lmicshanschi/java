package Chapter3.Tasks.Task11;

import java.util.Scanner;

import Chapter3.Tasks.Task11.Account;

public class AccountTest {
	
public static void main(String[] args) {
		
		
		
		Account account1 = new Account("Jane Green", 50.00);
		Account account2 = new Account("John Blue", -7.35);
		
		System.out.printf("%s balance: $%.2f%n", account1.getName(), account1.getBalance());
		System.out.printf("%s balance: $%.2f%n", account2.getName(), account2.getBalance());
		
		Scanner input = new Scanner(System.in);
		
		System.out.printf("Enter deposit amount for account1: ");
		double depositAmount = input.nextDouble();
		System.out.printf("%nadding %.2f to account1 balance:%n%n ", depositAmount);
		account1.deposite(depositAmount);
		
		System.out.printf("%s balance: $%.2f%n",account1.getName(), account1.getBalance());
		System.out.printf("%s balance: $%.2f%n%n",account2.getName(), account2.getBalance());
		
		System.out.printf("Enter deposit amount for account2: ");
		depositAmount = input.nextDouble();
		System.out.printf("%nadding %.2f to account2 balance:%n%n ", depositAmount);
		account2.deposite(depositAmount);
		
		System.out.printf("%s balance: $%.2f%n",account1.getName(), account1.getBalance());
		System.out.printf("%s balance: $%.2f%n%n",account2.getName(), account2.getBalance());
		
		System.out.printf("Enter withdraw for account1: ");
		double withdrawMoney = input.nextDouble();
		account1.withdraw(withdrawMoney);
		
		System.out.printf("Enter withdraw for account2: ");
		withdrawMoney = input.nextDouble();
		account2.withdraw(withdrawMoney);
		
		System.out.printf("%s balance: $%.2f%n",account1.getName(), account1.getBalance());
		System.out.printf("%s balance: $%.2f%n%n",account2.getName(), account2.getBalance());
	}

}
