package Chapter3.Tasks.Task12;

public class Invoice {
	
	private String partNumber;
	private String description;
	private int quantity;
	private double price;
	
	
	public Invoice (String partNumber, String description,int quantity, double price){
		
		this.partNumber = partNumber;
		this.description = description;
		
		if (quantity > 0.0) {
			this.quantity = quantity;
		}
		
		if (price > 0.0) {
			this.price = price;
		}
		


		
		
	}
	
	public void setPartNumber(String pnumber){

		this.partNumber = partNumber;

	}
	
	public String getPartNumber() {
		return partNumber;
	}
	
	public void setDescription(String description){

		this.description = description;

	}
	
	public String getDescription() {
		return description;
	}
	
	public void setQuantity(int quantity){
		if(quantity > 0) {
			this.quantity = quantity;
		}

	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setPrice(double price){
		if(price > 0) {
			this.price = price;
		}

	}
	
	public double getPrice() {
		return price;
	}
	
	public double getInvoiceAmount() {
		double sum = 0;
		if(quantity > 0 && price > 0){
			sum = quantity * price;
		}
		
		return sum;
	}

}
