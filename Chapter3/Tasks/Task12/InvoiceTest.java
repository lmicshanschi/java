package Chapter3.Tasks.Task12;

import java.util.Scanner;

public class InvoiceTest {
	
	public static void main(String[] args) {
		
		Invoice invoice1 = new Invoice("010", "Metal part of robot", 0, 0.0);
		
		System.out.printf("Number of part:%s%n", invoice1.getPartNumber());
		System.out.printf("Description of the part:%s%n", invoice1.getDescription());
		
		Scanner input = new Scanner(System.in);
		
		System.out.printf("Enter quantity of a part: ");
		int quantity = input.nextInt();
		invoice1.setQuantity(quantity);
		System.out.printf("Enter Price of a part:$ ");
		double price = input.nextDouble();
		invoice1.setPrice(price);
		
		System.out.printf("Quantyty of a parts:%d%n ", invoice1.getQuantity());
		System.out.printf("Price of a parts:$%.2f%n ", invoice1.getPrice());
		System.out.printf("Invoice amount:$%.2f%n",invoice1.getInvoiceAmount());
	}

}
