package Chapter3.Tasks.Task14;

import java.util.Scanner;

public class DateTest {
	
	public static void main(String[] args) {
		
		Date date1 = new Date(0,0,0);
		
		Scanner input = new Scanner(System.in);
		
		System.out.printf("Enter a month: ");
		int month = input.nextInt();
		date1.setMonth(month);
		System.out.printf("Month is:%d%n ", date1.getMonth());
		
		System.out.printf("Enter a day: ");
		int day = input.nextInt();
		date1.setDay(day);
		System.out.printf("Month is:%d%n ", date1.getDay());
		
		System.out.printf("Enter a year: ");
		int year = input.nextInt();
		date1.setYear(year);
		System.out.printf("Year is:%d%n ", date1.getYear());
		
		System.out.print("Full date is:");
		date1.displayDate();
	}

}
