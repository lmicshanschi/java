package Chapter3.Tasks.Task13;

public class EmployeeTest {
	
	public static void main(String[] args) {
		
		Employee employee1 = new Employee("John","Doe", 100000.00);
		Employee employee2 = new Employee("Sarah","Connor", 70000.00);
		
		System.out.printf("Employee1:%s\t%s%n",employee1.getFirstName(),employee1.getLastName());
		System.out.printf("Employee1 salary:$%.2f%n",employee1.getSalary());
		
		System.out.printf("Employee2:%s\t%s%n",employee2.getFirstName(),employee2.getLastName());
		System.out.printf("Employee2 salary:$%.2f%n",employee2.getSalary());
		
		employee1.setSalary(employee1.getSalary() + (employee1.getSalary() * 0.1 ));
		employee2.setSalary(employee2.getSalary() + (employee2.getSalary() * 0.1 ));
		
		System.out.printf("Employee1:%s\t%s%n",employee1.getFirstName(),employee1.getLastName());
		System.out.printf("Employee1 salary after raising:$%.2f%n",employee1.getSalary());
		
		System.out.printf("Employee2:%s\t%s%n",employee2.getFirstName(),employee2.getLastName());
		System.out.printf("Employee2 salary after raising:$%.2f%n",employee2.getSalary());
	}

}
