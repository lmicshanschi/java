package Chapter20.Tasks.Task5;

public class OverloadedMethodprintArray {
	
	public static <T> void printArray(T[] inputArray,
			int lowSubscript, int highSubscript) throws Exception {
		if (lowSubscript < 0 || highSubscript <= inputArray.length)
			throw new Exception();
		for (T element : inputArray)
			System.out.printf("%s ", element);
		System.out.println();
	}

}
