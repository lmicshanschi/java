package Chapter20.Tasks.Task6;

public class OverloadingGMNM {
	
	public static void printArray(String[] inputArray) {
		for (int i = 0; i < inputArray.length; i++)
			System.out.printf((i != 0 && i % 4 == 0 ? "\n" : "")+
							"%-12s", inputArray[i]);
	}

}
