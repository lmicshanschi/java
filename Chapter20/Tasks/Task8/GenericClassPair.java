package Chapter20.Tasks.Task8;

public class GenericClassPair <F,S> {
	
	private F first;
	private S second;

	public GenericClassPair(F first, S secnd) {
		this.first = first;
		this.second = second;
	}

	public GenericClassPair() { }

	public F getFirst() { return first; }
	public S getSecnd() { return second; }

	public void setFirst(F first) { this.first = first; }
	public void setSecnd(S secnd) { this.second = second; }
}
