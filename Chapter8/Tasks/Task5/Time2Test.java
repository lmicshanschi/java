package Chapter8.Tasks.Task5;

public class Time2Test {
	public static void main(String[] args) {
		Time2 t1 = new Time2(00, 24, 12);

		String t2 = t1.toUniversalString();

		String t3 = t1.toString();

		System.out.println("Universal-time Format (HH:MM:SS):" + t2);
		System.out.println("Standard-time Format (H:MM:SS AM or PM):" + t3);

		t1.midnightToString(t3);

	}

}
