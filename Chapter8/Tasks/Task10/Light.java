package Chapter8.Tasks.Task10;

public enum Light {
	
	RED(5),  
    GREEN(120),  
    YELLOW(30);

    private final int duration; 

    Light(int duration) { 
        this.duration = duration; 
    }  

    public int getDuration() { 
        return this.duration; 
    }

}
