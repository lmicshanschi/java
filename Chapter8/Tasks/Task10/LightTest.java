package Chapter8.Tasks.Task10;

public class LightTest {

	public static void main(String[] args) {

		for (Light light : Light.values()) {
			System.out.println("The traffic light value is: " + light);
			System.out.println("The duration of that trafic light value is: " + light.getDuration());
		}
	}

}
