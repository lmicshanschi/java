package Chapter8.Tasks.Task4;

public class RectangleTest {

	public static void main(String[] args) {

		Rectangle r = new Rectangle();

		System.out.println(" Test 1 - Valid length and width");
		float l = r.setLength(5);
		float w = r.setWidth(2);
		float a = r.calcArea(l, w);
		float p = r.calcPerimeter(l, w);
		System.out.println("Area:" + a + "Perimeter: " + p);

		System.out.println();

		System.out.println(" Test 2 - Invalid length and width");
		float l2 = r.setLength(21);
		float w2 = r.setWidth(-1);
		float a2 = r.calcArea(l2, w2);
		float p2 = r.calcPerimeter(l2, w2);
		System.out.println("Area:" + a2 + "Perimeter:" + p2);

	}
}
