package Chapter8.Tasks.Task4;

public class Rectangle {

	private float length = 1;
	private float width = 1;

	public float getLength() {
		return length;
	}

	public float setLength(float length) {
		if ((length > 0.0) && (length < 20.0)) {
			return length;
		} else {
			System.out.printf("Invalid length (%.2f) set to 1.", length);
			return 1;
		}
	}

	public float getWidth() {
		return width;
	}

	public float setWidth(float width) {
		if ((width > 0.0) && (width < 20.0)) {
			return width;
		} else {
			System.out.printf("Invalid width (%.2f) set to 1.", width);
			return 1;
		}
	}

	public float calcPerimeter(float length, float width) {
		float perimeter = 2 * (length + width);
		return perimeter;
	}

	public float calcArea(float length, float width) {
		float area = length * width;
		return area;
	}

}
