package Chapter8.Tasks.Task6;

public class SavingsAccountTest {

	public static void main(String args[]) {

		SavingsAccount save1 = new SavingsAccount(2000);
		SavingsAccount save2 = new SavingsAccount(3000);
		SavingsAccount.setAnnualInterestRate(0.04f);

		System.out.println("S1: " + save1);
		System.out.println("S2: " + save2);

		SavingsAccount.setAnnualInterestRate(0.05f);
		System.out.println("S1: " + save1);
		System.out.println("S2: " + save2);
	}

}
