package Chapter8.Tasks.Task6;

public class SavingsAccount {

	private static float annualInterestRate = 0f;
	private float savingsBalance;

	public SavingsAccount(float balance) {
		savingsBalance = balance;
	}

	public static void setAnnualInterestRate(float t) {
		if (t >= 0 && t <= 1)
			annualInterestRate = t;
		else
			throw new IllegalArgumentException("Annual interest rate should be between 0 and 1");
	}

	private float calculateMonthlyInterest() {
		return savingsBalance * annualInterestRate / 12;
	}

	public float getSavingsBalance() {
		return savingsBalance + calculateMonthlyInterest();
	}

	public float getAnnualInterestRate() {
		return annualInterestRate;
	}

	public String toString() {
		return String.format("Balance: %.2f", getSavingsBalance());
	}

}
