package Chapter2.Tasks;

import java.util.Scanner;

public class Task32 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int num1;
		int num2;
		int num3;
		int num4;
		int num5;

		int countNegative = 0;
		int countPositive = 0;
		int countZero = 0;

		int[] numberArray = new int[5];

		System.out.print("Enter Your First Number: ");
		num1 = input.nextInt();
		numberArray[0] = num1;
		System.out.print("Enter Your Second Number: ");
		num2 = input.nextInt();
		numberArray[1] = num2;
		System.out.print("Enter Your Third Number: ");
		num3 = input.nextInt();
		numberArray[2] = num3;
		System.out.print("Enter Your Fourth Number: ");
		num4 = input.nextInt();
		numberArray[3] = num4;
		System.out.print("Enter Your Fifth Number: ");
		num5 = input.nextInt();
		numberArray[4] = num5;

		for (int i = 0; i < numberArray.length; i++) {

			if (numberArray[i] < 0) {
				countNegative++;

			}
			if (numberArray[i] > 0) {
				countPositive++;

			}
			if (numberArray[i] == 0) {
				countZero++;

			}

		}
		System.out.printf("Number of negative digits %d%n", countNegative);
		System.out.printf("Number of positive digits %d%n", countPositive);
		System.out.printf("Number of zero digits %d%n", countZero);

	}

}
