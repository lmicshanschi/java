package Chapter2.Tasks;

import java.util.Scanner;

public class Task28 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int r;
		int diameter;
		double circumference;
		double area;

		System.out.print("Enter radius: ");
		r = input.nextInt();

		diameter = 2 * r;
		circumference = 2 * (Math.PI * r);
		area = Math.PI * (r * r);

		System.out.printf("Diameter is: %d%n", diameter);
		System.out.printf("Circumference is: %f%n", circumference);
		System.out.printf("Area is: %f%n", area);

	}

}
