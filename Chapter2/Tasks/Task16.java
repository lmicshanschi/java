package Chapter2.Tasks;

import java.util.Scanner;

public class Task16 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int x;
		int y;

		System.out.print("Enter first integer: ");
		x = input.nextInt();

		System.out.print("Enter second integer: ");
		y = input.nextInt();

		if (x > y) {
			System.out.printf("Is larger %d%n", x);
		} else if (y > x) {
			System.out.printf("Is larger %d%n", y);
		} else {
			System.out.printf("These numbers are equal");
		}

	}

}
