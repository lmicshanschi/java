package Chapter2.Tasks;

import java.util.Scanner;

public class Task17 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int x;
		int y;
		int z;
		int sum;
		int product;
		int average;
		int max;
		int min;

		System.out.print("Enter first integer: ");
		x = input.nextInt();

		System.out.print("Enter second integer: ");
		y = input.nextInt();

		System.out.print("Enter third integer: ");
		z = input.nextInt();

		sum = x + y + z;
		product = x * y * z;
		average = Math.round((x + y + z) / 3);
		max = maxNumber(x, y, z);
		min = minNumber(x, y, z);

		System.out.printf("Sum is %d%n", sum);
		System.out.printf("Product is %d%n", product);
		System.out.printf("Average is %d%n", average);
		System.out.printf("Is larger is %d%n", max);
		System.out.printf("Is smallest is %d%n", min);

	}

	public static int maxNumber(int a, int b, int c) {
		int max;
		if (a > b && a > c) {
			max = a;
		} else if (b > a && b > c) {
			max = b;
		} else {
			max = c;
		}

		return max;
	}

	public static int minNumber(int a, int b, int c) {
		int min;
		if (a < b && a < c) {
			min = a;
		} else if (b < a && b < c) {
			min = b;
		} else {
			min = c;
		}

		return min;
	}

}
