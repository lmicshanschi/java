package Chapter2.Tasks;

import java.util.Scanner;

public class Task25 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int x;

		System.out.print("Enter first integer: ");
		x = input.nextInt();

		if (x % 2 == 0) {
			System.out.printf("Number is odd %d%n", x);
		} else {
			System.out.printf("Number is even %d%n", x);
		}
	}

}
