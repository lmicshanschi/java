package Chapter2.Tasks;

import java.util.Scanner;

public class Task30 {
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int num;
		String newStr = "";
		String space = " ";

		System.out.print("Enter a number with five digits: ");
		num = input.nextInt();

		String str = String.valueOf(num);

		if (str.length() != 0 && str.length() == 5) {
			for (int i = 0; i < str.length(); i++) {
				newStr += str.charAt(i) + space;

			}
			System.out.printf(newStr);
		} else {
			System.out.printf("Please enter only five digits!");
		}

	}

}
