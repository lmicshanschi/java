package Chapter2.Tasks;

import java.util.Scanner;

public class Task24 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		int x;
		int y;
		int z;
		int k;
		int j;

		System.out.print("Enter first integer: ");
		x = input.nextInt();

		System.out.print("Enter second integer: ");
		y = input.nextInt();

		System.out.print("Enter third integer: ");
		z = input.nextInt();

		System.out.print("Enter fourth integer: ");
		k = input.nextInt();

		System.out.print("Enter fifth integer: ");
		j = input.nextInt();

		if (x > y && x > z && x > k && x > j) {
			System.out.printf("Is larger is %d%n", x);
		}

		if (y > x && y > z && y > k && y > j) {
			System.out.printf("Is larger is %d%n", y);
		}

		if (z > x && z > y && z > k && z > j) {
			System.out.printf("Is larger is %d%n", z);
		}

		if (k > x && k > y && k > z && k > j) {
			System.out.printf("Is larger is %d%n", k);
		}

		if (j > x && j > y && j > z && j > k) {
			System.out.printf("Is larger is %d%n", j);
		}

		if (x < y && x < z && x < k && x < j) {
			System.out.printf("Is smallest is %d%n", x);
		}

		if (y < x && y < z && y < k && y < j) {
			System.out.printf("Is smallest is %d%n", y);
		}

		if (z < x && z < y && z < k && z < j) {
			System.out.printf("Is smallest is %d%n", z);
		}

		if (k < x && k < y && k < z && k < j) {
			System.out.printf("Is smallest is %d%n", k);
		}

		if (j < x && j < y && j < z && j < k) {
			System.out.printf("Is smallest is %d%n", j);
		}

	}

}
