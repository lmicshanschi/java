package Chapter14.Tasks.Task14;

public class SearchED {
	
	public static void main(String[] args){
		if (args.length < 1)
			return;
		final String[] tokens = args[0].split("\\s+");

		for (String s : tokens)
			if (s.endsWith("ED"))
				System.out.print(s + " ");
		System.out.println();
	}

}
