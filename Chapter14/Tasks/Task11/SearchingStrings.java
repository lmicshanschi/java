package Chapter14.Tasks.Task11;

import java.util.Scanner;

public class SearchingStrings {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Enter string: ");
		String string = input.nextLine();
		System.out.print("Enter character: ");
		char letter = input.nextLine().charAt(0);
		System.out.println("Index: "+string.indexOf(letter));
	}

}
