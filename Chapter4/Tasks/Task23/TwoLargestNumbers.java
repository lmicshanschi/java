package Chapter4.Tasks.Task23;

import java.util.Arrays;
import java.util.Scanner;


public class TwoLargestNumbers {
	
public static class Largest {
		
		private int number;
		private int firstLargest;
		private int secondLargest;
		
		public Largest(int number, int firstLargest, int secondLargest) {
			
			this.number = number;
			this.firstLargest = firstLargest;
			this.secondLargest = secondLargest;
			
		}
		
		public void setNumber (int number) {
			this.number = number;
		}
		
		public int getNumber() {
			return number;
		}
		
		public void setFirstLargest (int firstLargest) {
			this.firstLargest = firstLargest;
		}
		
		public int getFirstLargest() {
			return firstLargest;
		}
		
		public void setSecondLargest (int secondLargest) {
			this.secondLargest = secondLargest;
		}
		
		public int getSecondLargest() {
			return secondLargest;
		}
	}
	
	public static void main(String args[]) {
		
		Largest larg = new Largest(0, 0, 0);
		
		int[] numbers;
		numbers = new int[10];
		int counter;
		int flg = 0;
		int slg = 0;
		Scanner input = new Scanner(System.in);
		
		for(counter = 0; counter < 10; counter++) {
			System.out.printf("Enter number: ");
			int num = input.nextInt();
			numbers[counter] += num;
			if(counter == 9) {
				int last = numbers[9];
				larg.setNumber(last);
			}
			}
		
		for(int i = 0; i < numbers.length; i++) {
			if(numbers[i] > flg) {
				flg = numbers[i];
				larg.setFirstLargest(flg);
			}for(int j = 0; j < numbers.length; j++)
	        {
	            if(numbers[j] > slg && numbers[j] < flg)
	            	slg = numbers[j];
	            larg.setSecondLargest(slg);
	        }
		    }
		
		System.out.println(Arrays.toString(numbers));
		System.out.printf("The last number is: %d%n", larg.getNumber());
		System.out.printf("The first large number is: %d%n", larg.getFirstLargest());
		System.out.printf("The second large number is: %d", larg.getSecondLargest());
		
		
	}

}
