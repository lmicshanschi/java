package Chapter4.Tasks.Task17;

import java.util.Scanner;

public class GasMileage {

	public static void main(String[] args) {

		int miles;
		int gallons;
		int totalMiles = 0;
		int totalGallons = 0;

		Scanner input = new Scanner(System.in);

		System.out.println("Enter miles: ");
		miles = input.nextInt();

		if (miles > 0) {
			System.out.print("Enter gallons used: ");
			gallons = input.nextInt();
			System.out.println("Miles driven for this trip: " + miles);
			System.out.println("Gallons used for this trip: " + gallons);
			totalMiles += miles;
			totalGallons += gallons;
			System.out.printf("Miles per gallon for this trip: %.2f%n",milesOnGallons(miles, gallons));
			System.out.printf("Combined Miles per gallon :%.2f ", combinedMiles(totalMiles, totalGallons));

		}

	}

	public static double milesOnGallons(int miles, int gallons) {
		double result = miles / gallons;
		return result;
	}

	public static double combinedMiles(int totalMiles, int totalGallons) {
		double result = totalMiles / totalGallons;
		return result;
	}

}
