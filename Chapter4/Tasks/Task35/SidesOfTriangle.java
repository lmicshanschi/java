package Chapter4.Tasks.Task35;

import java.util.Scanner;


public class SidesOfTriangle {
	
	public static class Triangle {
		int a; 
		int b;
		int c;
		
		public Triangle(int a, int b, int c) {
			this.a = a;
			this.b = b;
			this.c = c;
		}
		
		public void setA (int a) {
			this.a = a;
		}
		
		public int getA() {
			return a;
		}
		public void setB (int b) {
			this.b = b;
		}
		
		public int getB() {
			return b;
		}
		
		public void setC (int c) {
			this.c = c;
		}
		
		public int getC() {
			return c;
		}
		
		public static void sideŅomparison(int a, int b, int c) {
			if(a < (b + c) &&  a > (b - c) &&  b < (a + c) &&  b > (a - c) &&  c < (a + b) &&  c > (a - b) ) {
				System.out.printf("Yes the are sides of triangle");
			}else {
				System.out.printf("No, the are not sides of triangle");
			}
		}
		}
	
	public static void main(String args[]) {
		
		Triangle triangle1 = new Triangle(0, 0, 0);
		
		Scanner input = new Scanner(System.in);
		
		System.out.printf("Please insert side a: %n");
		int a = input.nextInt();
		triangle1.setA(a);
		
		System.out.printf("Please insert side b: %n");
		int b = input.nextInt();
		triangle1.setB(b);
		
		System.out.printf("Please insert side a: %n");
		int c = input.nextInt();
		triangle1.setC(c);
		
		triangle1.sideŅomparison(a, b, c);
	}
}
