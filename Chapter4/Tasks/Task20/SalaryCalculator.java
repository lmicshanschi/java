package Chapter4.Tasks.Task20;

import java.util.Scanner;

public class SalaryCalculator {
	
	public static class Employee {
		
		private String firstName;
		private String lastName;
		private double allHours;
		private double hourRate;
		
		public Employee(String firstName, String lastName,double allHours, double hourRate) {
			
			this.firstName = firstName;
			this.lastName = lastName;
			if(allHours > 0) {
			this.allHours = allHours;
			}
			if(hourRate > 0) {
			this.hourRate = hourRate;
			}
			
		}
		
		public void setFirstName (String firstName) {
			this.firstName = firstName;
		}
		
		public String getFirstName() {
			return firstName;
		}
		
		public void setLastName (String lastName) {
			this.lastName = lastName;
		}
		
		public String getLastName() {
			return lastName;
		}
		
		public void setAllHours(double allHours) {
			if(allHours > 0) {
				this.allHours = allHours;
			}
		}
		
		public double getAllHours() {
			return allHours;
		}
		
		public void setHourRate(double hourRate) {
			if(hourRate > 0) {
				this.hourRate = hourRate;
			}
		}
		
		public double getHourRate() {
			return hourRate;
		}
		
		public double grossPay(double allHours, double hourRate) {
			double result;
			if(allHours <= 40) {
				result = allHours * hourRate;
			}else {
				result = ((allHours - (allHours - 40))* hourRate + ((allHours - 40) * (hourRate / 2)));
			}
			
			return result;
			
		}
		
	}
	
	public static void main(String args[]) {
		
		Employee employee1 = new Employee("John", "Doe", 45.00, 8.00);
		
		System.out.printf("Employee data: %s %s%n ", employee1.getFirstName(), employee1.getLastName());
		System.out.printf("Hour rate: %.2f%n", employee1.grossPay(employee1.getAllHours(),employee1.getHourRate()));
		
		Scanner input = new Scanner(System.in);
		
		System.out.printf("Enter hours for employee1: ");
		double allHours = input.nextDouble();
		employee1.setAllHours(allHours);
		System.out.printf("Enter hour rate for employee1: ");
		double hourRate = input.nextDouble();
		employee1.setHourRate(hourRate);
		
		System.out.printf("Employee data: %s %s%n ", employee1.getFirstName(), employee1.getLastName());
		System.out.printf("Hour rate: %.2f%n", employee1.grossPay(employee1.getAllHours(),employee1.getHourRate()));
		
		
		
		
		
		
	}

}
