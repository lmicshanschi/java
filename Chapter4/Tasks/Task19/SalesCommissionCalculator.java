package Chapter4.Tasks.Task19;

public class SalesCommissionCalculator {

	private double BasicEarnings;
	private double MerchandisePlus;
	private double CurrentMerchandise;
	
	public SalesCommissionCalculator ( double BasicEarnings, double MerchandisePlus) {
		
		this.BasicEarnings = BasicEarnings;
		this.MerchandisePlus = MerchandisePlus;
	}

	public void setMerchandisePlus(double inMPlus) {
		MerchandisePlus = inMPlus;
	}

	public void setBasicEarnings(double inBEarnings) {
		BasicEarnings = inBEarnings;
	}

	public void CommissionBasis(double inBEarnings, double inMPlus) {
		setBasicEarnings(inBEarnings);
		setMerchandisePlus(inMPlus);
	}

	public void setCurrentMerchandise(double inCMerchandise) {
		CurrentMerchandise = inCMerchandise;
	}

	public double getCurrentMerchandise() {
		return CurrentMerchandise;
	}

	public double TotalBasis() {
		double result = CurrentMerchandise * MerchandisePlus + BasicEarnings;
		return result;
	}

}
