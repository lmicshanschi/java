package Chapter4.Tasks.Task19;

import java.util.Scanner;

public class companyPays {

	public static void main(String args[]) {
		double BasicEarning = 200.0;
		double MerchandisePlus = 0.09;
		int section = 5;
		Scanner input = new Scanner(System.in);

		System.out.printf("The salespeople's basic earnings is $%.2f, merchandise plus is %.2f %%.\n", BasicEarning,
				MerchandisePlus * 100.0);
		SalesCommissionCalculator saler = new SalesCommissionCalculator(BasicEarning, MerchandisePlus);

		do {
			System.out.println("Item\tValue ");
			System.out.println("1\t239.99 ");
			System.out.println("2\t129.75 ");
			System.out.println("3\t99.95 ");
			System.out.println("4\t350.89 ");

			System.out.print("\nSelect sales item(Enter 0 to leave):");
			section = input.nextInt();

			switch (section) {
			case 1:
				saler.setCurrentMerchandise(saler.getCurrentMerchandise() + 239.99);
				break;

			case 2:
				saler.setCurrentMerchandise(saler.getCurrentMerchandise() + 129.75);
				break;

			case 3:
				saler.setCurrentMerchandise(saler.getCurrentMerchandise() + 99.95);
				break;

			case 4:
				saler.setCurrentMerchandise(saler.getCurrentMerchandise() + 350.89);
				break;

			case 0:
			default:

				break;
			}
			System.out.printf("Current merchandise is: $%.2f \n\n", saler.getCurrentMerchandise());

		} while (section != 0);

		System.out.printf("The salespeople's total basis you need to pay is : $%.2f", saler.TotalBasis());

	}

}
