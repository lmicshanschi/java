package Chapter4.Tasks.Task31;

import java.util.Scanner;

public class DecimalEquivalentBinaryNumber {
	
	public static class Binary {
		String binaryDigit;
		int a; 
		int b;
		
		public Binary(String binaryDigit, int a, int b) {
			this.binaryDigit = binaryDigit;
			this.a = a;
			this.b = b;
		}
		
		public void setBinaryDigit (String binaryDigit) {
			this.binaryDigit = binaryDigit;
		}
		
		public String getBinaryDigit() {
			return binaryDigit;
		}
		
		 public static int pow(int a, int b) {
		        int result = 1;
		        for (int i = 0; i < b; i++) {
		            result *= a;
		        }
		        return result;
		    }
		 
		   public static int BinToDec(String bin){
		       int res = 0, a = 0, mult = 0;
		       char[] symbols = bin.toCharArray();
		       for(int len = symbols.length-1; len >= 0; len--){
		           int temp = 0;
		           a = Character.getNumericValue(symbols[len]);
		           temp = a * pow(2, mult);
		           mult++;
		           res += temp;
		       }
		       return res;
		   }
	}
	
	public static void main(String args[]) {
		
		Binary binary1 = new Binary("", 0, 0); 
		
		Scanner input = new Scanner(System.in);
		
		System.out.printf("Binary digit should contain only 0 and 1: %n");
		System.out.printf("Please enter binary digit: ");
		
		String bg = input.nextLine();
		binary1.setBinaryDigit(bg);
		
		System.out.printf("The binary digit is: %s %n", binary1.getBinaryDigit());
		String bg2 = binary1.getBinaryDigit();
		System.out.printf("The decimal digit is: %d ", binary1.BinToDec(bg2));
	}

}
