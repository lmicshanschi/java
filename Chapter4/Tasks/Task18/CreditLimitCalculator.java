package Chapter4.Tasks.Task18;

import java.util.Scanner;

public class CreditLimitCalculator {

	public static void main(String[] args) {

		int accountNumber;
		int balance;
		int totalItems;
		int totalCredits;
		int allowedCreditLimit;

		Scanner input = new Scanner(System.in);

		System.out.printf("Enter \"customer account number\": %n");
		accountNumber = input.nextInt();

		System.out.printf("Enter \"customer balance\" at the  beginning of the month: %n");
		balance = input.nextInt();

		System.out.printf("Enter \"total of all items charged\" by the Customer this month: %n");
		totalItems = input.nextInt();

		System.out.printf("Enter \"total of all credits applied\" to the Customer this month: %n");
		totalCredits = input.nextInt();

		System.out.printf("Enter the \"allowed credit limit\" of the Customer: %n");
		allowedCreditLimit = input.nextInt();

		int newBalance = balance + totalItems - totalCredits;

		displayInfo(accountNumber, balance, totalItems, totalCredits, allowedCreditLimit);

		System.out.printf("The Customer's new Balance is: %d%n", newBalance);

		if (newBalance < allowedCreditLimit) {
			System.out.println("Credit limit exceeded");
		}

	}

	public static void displayInfo(int accountNumber, int balance, int totalItems, int totalCredits,
			int allowedCreditLimit) {
		System.out.println("Customer info:");
		System.out.printf("Customer number: %d%n", accountNumber);
		System.out.printf("Begining month balance: %d%n", balance);
		System.out.printf("Total number of items charged this month: %d%n", totalItems);
		System.out.printf("Total number of Credits Applied this month: %d%n", totalCredits);
	}

}
