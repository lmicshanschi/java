package Chapter4.Tasks.Task24;

import java.util.Scanner;

public class ValidatingUserInput {
	
	public static void main(String args[]) {
		
		int number;
		
		Scanner input = new Scanner(System.in);
		
		System.out.printf("Enter number: ");
		number = input.nextInt();
		
		while(number != 2 || number != 1) {
			System.out.printf("Please enter correct number: ");
			number = input.nextInt();
			if(number == 2 || number == 1) {
				System.out.printf("This is correct number.Welcome!");
				break;
			}
			
		}
		
		
	}

}
