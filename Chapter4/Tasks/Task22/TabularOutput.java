package Chapter4.Tasks.Task22;

public class TabularOutput {

	public static void main(String args[]) {
		
		for(int i = 1; i <= 5 ; i++){
			System.out.print(i + "\t");
			System.out.print(i*10 + "\t");
			System.out.print(i*100 + "\t");
			System.out.println(i*1000);
			}
	}
}
