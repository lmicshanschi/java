package Chapter4.Tasks.Task37;

import java.util.Scanner;

public class EtoX {

	public void approximate() {
		Scanner input = new Scanner(System.in);

		int number;
		int accuracy;
		int factorial;
		int x;
		double e;
		double exponent;

		number = 1;
		factorial = 1;
		e = 1.0;
		exponent = 1.0;

		System.out.print("Enter exponent: ");
		x = input.nextInt();

		System.out.print("Enter desired accuracy of e: ");
		accuracy = input.nextInt();

		while (number < accuracy) {
			exponent *= x;
			factorial *= number;
			e += exponent / factorial;
			number++;
		}

		System.out.printf("e to the %d is ", x);
		System.out.println(e);
	}
}
