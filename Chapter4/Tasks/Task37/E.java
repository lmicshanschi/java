package Chapter4.Tasks.Task37;

import java.util.Scanner;

public class E {

	public void approximate() {
		Scanner input = new Scanner(System.in);

		int number; // counter
		int accuracy; // accuracy of estimate
		int factorial; // value of factorial
		double e; // estimate value of e

		number = 1;
		factorial = 1;
		e = 1.0;

		System.out.print("Enter desired accuracy of e: ");
		accuracy = input.nextInt();

		while (number < accuracy) {
			factorial *= number;
			e += 1.0 / factorial;
			number++;
		}

		System.out.print("e is ");
		System.out.println(e);
	}

}
