package Chapter4.Tasks.Task37;

import java.util.Scanner;

public class Factorial {
	
	public static class factorialFormula {
		int factorial;
		int e;
		int x;
		
		public factorialFormula(int factorial, int e, int x) {
			this.factorial = factorial;
			this.e = e;
			this.x = x;
		}
		
		public void setFactorial (int factorial) {
			this.factorial = factorial;
		}
		
		public int getFactorial() {
			return factorial;
		}
		public void setE (int e) {
			this.e = e;
		}
		
		public int getE() {
			return e;
		}
		
		public void setX (int x) {
			this.x = x;
		}
		
		public int getX() {
			return x;
		}
		
		public static int factorialComput(int factorial) {
			int result = 1;
			if(factorial != 0 && factorial > 0) {
			for(int i = 1; i <= factorial; i++) {
				result *= i;
			}
			
			
			} else {
				System.out.printf("Please enter nonnegative integer: ");
			}
			return result;
		}
	}
	
	public static void main(String args[]) {
		
		factorialFormula factorial1 = new factorialFormula(0, 0, 0);
		
		Scanner input = new Scanner(System.in);
		
		System.out.printf("Please enter factorial: ");
		
		int factorial = input.nextInt();
		factorial1.setFactorial(factorial);
		
		System.out.printf("Result is: %d", factorial1.factorialComput(factorial));
		
	}

}
