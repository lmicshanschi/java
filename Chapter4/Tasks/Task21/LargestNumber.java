package Chapter4.Tasks.Task21;

import java.util.Scanner;
import java.util.Arrays;

public class LargestNumber {

	public static class Largest {
		
		private int number;
		private int largest;
		
		public Largest(int number, int largest) {
			
			this.number = number;
			this.largest = largest;
			
		}
		
		public void setNumber (int number) {
			this.number = number;
		}
		
		public int getNumber() {
			return number;
		}
		
		public void setLargest (int largest) {
			this.largest = largest;
		}
		
		public int getLargest() {
			return largest;
		}
	}
	
	public static void main(String args[]) {
		
		Largest larg = new Largest(0, 0);
		
		int[] numbers;
		numbers = new int[10];
		int counter;
		int lg = 0;
		Scanner input = new Scanner(System.in);
		
		for(counter = 0; counter < 10; counter++) {
			System.out.printf("Enter number: ");
			int num = input.nextInt();
			numbers[counter] += num;
			if(counter == 9) {
				int last = numbers[9];
				larg.setNumber(last);
			}
			}
		
		for(int i = 0; i < numbers.length; i++) {
			if(numbers[i] > lg) {
				lg = numbers[i];
				larg.setLargest(lg);
			}
		}
		System.out.println(Arrays.toString(numbers));
		System.out.printf("The last number is: %d%n", larg.getNumber());
		System.out.printf("The large number is: %d", larg.getLargest());
		
		
	}
	
}
