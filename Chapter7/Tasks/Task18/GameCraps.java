package Chapter7.Tasks.Task18;

import java.util.Random;

public class GameCraps {


	private Random randomNumbers = new Random();

	private enum Status { CONTINUE, WON, LOST };

	int wins[];
	int losses[];
	int winSum = 0;
	int loseSum = 0;

	public void play() {
		int sumOfDice = 0;
		int myPoint = 0;

		Status gameStatus;

		int roll;

		wins = new int[22];
		losses = new int[22];

		for (int i = 1; i <= 1000000; i++) {
			sumOfDice = rollDice();
			roll = 1;

			switch (sumOfDice) {
			case 7:
			case 11:
				gameStatus = Status.WON;
				break;
			
			case 2:
			case 3:
			case 12:
				gameStatus = Status.LOST;
				break;
			
			default:
				gameStatus = Status.CONTINUE;
				myPoint = sumOfDice;
				break;
			}

			while (gameStatus == Status.CONTINUE) {
				sumOfDice = rollDice();
				roll++;

				
				if (sumOfDice == myPoint)
					gameStatus = Status.WON;
				else if (sumOfDice == 7)
					gameStatus = Status.LOST;
			}

			if (roll > 21)
				roll = 21;

			if (gameStatus == Status.WON) {
				++wins[roll];
				++winSum;
			} else {
				++losses[roll];
				++loseSum;

			}
		}

		printStats();
	}

	public void printStats() {
		int totalGames = winSum + loseSum;
		int length = 0;

		for (int i = 1; i <= 21; i++) {
			if (i == 21)
				System.out.printf("%d %s %d %s\n", wins[i], "games won and", losses[i],
						"games lost on rolls after the 20th roll");
			else
				System.out.printf("%d %s %d %s%d\n", wins[i], "games won and", losses[i], "games lost on roll #", i);

			length += wins[i] * i + losses[i] * i;
		}

		System.out.printf("\n%s %d / %d = %.2f%%\n", "The chances of winning are", winSum, totalGames,
				(100.0 * winSum / totalGames));

		System.out.printf("The average game length is %.2f rolls.\n", ((double) length / totalGames));
	}

	public int rollDice() {

		int die1 = 1 + randomNumbers.nextInt(6);
		int die2 = 1 + randomNumbers.nextInt(6);
		int sum = die1 + die2;

		return sum;
	}

}
