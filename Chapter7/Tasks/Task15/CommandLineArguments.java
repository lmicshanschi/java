package Chapter7.Tasks.Task15;

public class CommandLineArguments {

	public static void main(String args[]) {
		int[] array;
		int size = 10;

		if (args.length == 1)
			size = Integer.parseInt(args[0]);

		array = new int[size];

		System.out.printf("%s%8s\n", "Index", "Value");

		for (int count = 0; count < array.length; count++)
			System.out.printf("%5d%8d\n", count, array[count]);
	}

}
