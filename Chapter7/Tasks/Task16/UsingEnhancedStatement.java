package Chapter7.Tasks.Task16;

public class UsingEnhancedStatement {

	public static void main(String args[]) {
		double total = 0.0;

		for (String argument : args)
			total += Double.parseDouble(argument);

		System.out.printf("total is: %.2f\n", total);
	}

}
