package Chapter16.Tasks.Task21;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Collections;

public class ChangingPriority {
	
	private final Color[] colors = 
		{Color.BLACK,	Color.BLUE,			Color.CYAN,		Color.DARK_GRAY,
		 Color.GREEN,	Color.LIGHT_GRAY,	Color.MAGENTA,	Color.ORANGE,
		 Color.PINK,	Color.RED,			Color.WHITE,	Color.YELLOW};
	private final String[] names =
		{"Black", "Blue", "Cyan", "Dark Gray", "Green",
			"Light Gray", "Magenta", "Orange", "Pink", "Red"};
	private final Map<String, Color> map = new HashMap<>(12);
	public void ColourPalette() {
		for (int i = 0; i < names.length; i++)
			map.put(names[i], colors[i]);
	}
	public Color getColor(String name) { return map.get(name); }
	PriorityQueue<Double> queue = new PriorityQueue<>(Collections.reverseOrder());

}
