package Chapter16.Tasks.Task14;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class CountingLetters {
	
	public static void main(String[] args)
	   {
	      
	      Map<String, Integer> myMap = new HashMap<>(); 

	      createMap(myMap); 
	      displayMap(myMap); 
	   } 

	   
	   private static void createMap(Map<String, Integer> map) 
	   {
	      Scanner scanner = new Scanner(System.in); 
	      System.out.println("Enter a string:"); 
	      String input = scanner.nextLine();

	    
		  Matcher mat = Pattern.compile("\\w").matcher(input);
	               
	     
		  while (mat.find())
	      {
	         String letter = mat.group().toLowerCase(); 
	                  
	         
	         if (map.containsKey(letter))
	         {
	            int count = map.get(letter);
	            map.put(letter, count + 1); 
	         } 
	         else 
	            map.put(letter, 1);
	      } 
	   } 
	   
	   
	   private static void displayMap(Map<String, Integer> map) 
	   {     
	      Set<String> keys = map.keySet(); // get keys

	      
	      TreeSet<String> sortedKeys = new TreeSet<>(keys);

	      System.out.printf("%nMap contains:%nKey\t\tValue%n");

	      
	      for (String key : sortedKeys)
	         System.out.printf("%-10s%10s%n", key, map.get(key));
	      
	      System.out.printf(
	         "%nsize: %d%nisEmpty: %b%n", map.size(), map.isEmpty());
	   } 

}
