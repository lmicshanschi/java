package Chapter10.Tasks.Task17;

public final class Building implements CarbonFootprint {

	private static final double BASE = 689610.0;
	private static final double RATE = 0.689551;
	private final double kWh;

	public Building(double kWh) {
		if (kWh < 0)
			throw new IllegalArgumentException("kWh must be greater than 0.");
		this.kWh = kWh;
	}

	public double getCarbonFootprint() {
		return kWh * RATE + BASE;
	}
}