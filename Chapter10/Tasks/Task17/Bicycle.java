package Chapter10.Tasks.Task17;

public final class Bicycle implements CarbonFootprint {
	public static double BASE = 240.4;

	public static double RATE = 0.3735;
	public final double km;

	public Bicycle(double km) {
		if (km < 0.0)
			throw new IllegalArgumentException("Kilograms must be greater than 0.");

		this.km = km;
	}

	public double getCarbonFootprint() {
		return km * RATE + BASE;
	}
}
