package Chapter10.Tasks.Task17;

public final class Car implements CarbonFootprint {
	private final double litres;
	
	private static final double BASE = 200000.0;
	private static final double RATE = 2.397; // kg/l

	public Car(double litres) {
		if (litres < 0)
			throw new IllegalArgumentException(
				"Litres must be greater than 0.");

		this.litres = litres;
	}

	public double getCarbonFootprint() { return litres * RATE + BASE; }
}