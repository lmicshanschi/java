package Chapter10.Tasks.Task13;

public abstract class ThreeDShape extends Shape {
	public abstract double getVolume();
}
