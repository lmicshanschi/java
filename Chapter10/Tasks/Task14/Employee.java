package Chapter10.Tasks.Task14;

import java.util.Date;

public abstract class Employee implements Payable{
	
	private final String firstName;
	private final String lastName;
	private final String socialSecurityNumber;

	
	private boolean dateSet = false; 
	private Date d;

	public void setDate(Date d) {
		if (!dateSet) {
			this.d = d;
			dateSet = true;
		}
	}

	public Date getDate() { return d; }

	public Employee(String firstName, String lastName,
			String socialSecurityNumber)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.socialSecurityNumber = socialSecurityNumber;
	}

	public String getFirstName() { return firstName; }

	public String getLastName() { return lastName; }

	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	@Override
	public String toString() {
		return String.format("%s %s%nsocial security number: %s",
				getFirstName(), getLastName(), getSocialSecurityNumber());
	}

	public abstract double earnings();

	public double getPaymentAmount() { return earnings(); }

}
