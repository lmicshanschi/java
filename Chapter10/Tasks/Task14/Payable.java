package Chapter10.Tasks.Task14;

public interface Payable {
	
	double getPaymentAmount();

}
