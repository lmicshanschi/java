package Chapter10.Tasks.Task12;

public interface Payable {
	
	double getPaymentAmount();

}
