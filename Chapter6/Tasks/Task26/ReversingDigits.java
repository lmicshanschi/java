package Chapter6.Tasks.Task26;

import java.util.Scanner;

public class ReversingDigits {

	public void reverseNum()

	{

		Scanner input = new Scanner(System.in);

		int num;

		int digit;

		int rev = 0;

		System.out.printf("Enter an integer:");

		num = input.nextInt();

		while (num != 0)

		{

			digit = num % 10;

			rev = rev * 10 + digit;

			num /= 10;

		}

		System.out.printf("Reverse integer is %d", rev);

	}

}
