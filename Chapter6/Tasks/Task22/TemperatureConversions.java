package Chapter6.Tasks.Task22;

import java.util.Scanner;

public class TemperatureConversions {

	public void convertTemperatures() {
		Scanner input = new Scanner(System.in);

		int choice; // the user's choice in the menu

		while (true) {
			// print the menu
			System.out.println("1. Fahrenheit to Celsius");
			System.out.println("2. Celsius to Fahrenheit");
			System.out.println("3. Exit");
			System.out.print("Choice: ");
			choice = input.nextInt();

			if (choice != 3) {
				System.out.print("Enter temperature: ");
				int oldTemperature = input.nextInt();

				// convert the temperature appropriately
				switch (choice) {
				case 1:
					System.out.printf("%d Fahrenheit is %d Celsius\n", oldTemperature, celsius(oldTemperature));
					break;

				case 2:
					System.out.printf("%d Celsius is %d Fahrenheit\n", oldTemperature, fahrenheit(oldTemperature));
					break;

				}

			}else {
				System.out.printf("Exit");
				break;
			}
		}

	}

	public int celsius(int fahrenheitTemperature) {
		return ((int) (5.0 / 9.0 * (fahrenheitTemperature - 32)));
	}

	public int fahrenheit(int celsiusTemperature) {
		return ((int) (9.0 / 5.0 * celsiusTemperature + 32));
	}

}
