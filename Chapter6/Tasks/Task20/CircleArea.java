package Chapter6.Tasks.Task20;

import java.util.Scanner;

public class CircleArea {
	
	public static void main(String[] args) 
	   {
	      double a;
	      double c;
	      System.out.println("Enter the radius: ");
	      Scanner input = new Scanner(System.in);
	      a = input.nextDouble();

	      c = circleArea(a);
	      System.out.println("Circle Area is: " + c);
	   }

	   public static double circleArea(double n1) {
	      double min;

	      min = (n1 * n1 * 3.14);

	      return min; 
	   }

}
