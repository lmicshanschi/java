package Chapter6.Tasks.Task29;

import java.util.Random;
import java.util.Scanner;

public class CoinTossing {

	private Random randomNumbers = new Random();

	public void flipCoins() {
		Scanner input = new Scanner(System.in);

		int heads = 0; 
		int tails = 0; 
		int choice; 

		while (true) {

			System.out.println("1. Toss Coin");
			System.out.println("2. Exit");
			System.out.print("Choice: ");
			choice = input.nextInt();
			if(choice != 2) {
			if (choice == 1) {
				if (flip())
					heads++;
				else
					tails++;

				System.out.printf("Heads: %d, Tails: %d\n", heads, tails);

			}
			}else {
				System.out.println("Exit");
				break;
			}
		} 
	}

	public boolean flip() {
		return randomNumbers.nextInt(2) == 1;
	}

}
