package Chapter6.Tasks.Task19;

import java.util.Scanner;

public class SquareAnyCharacter {
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Please insert side");
		int side = input.nextInt();
		System.out.println("Please insert character");
		char fillCharacter = input.next().charAt(0);
        SquareOfCharacters(side, fillCharacter);
    }
    
    public static void SquareOfCharacters (int side, char fillCharacter){
        for (int i=1; i<=side;i++){
            for(int j=1; j<=side;j++)
                System.out.print(fillCharacter);
            System.out.println();
        }
    }

}
