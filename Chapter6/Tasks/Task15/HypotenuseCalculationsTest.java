package Chapter6.Tasks.Task15;

public class HypotenuseCalculationsTest {
	
	public static void main(String[]args)
	{
		HypotenuseCalculations h = new HypotenuseCalculations();
		
		System.out.println("The hypotenuse of Triangle 1 is :"+h.Hypo(3.0,4.0));
		
		System.out.println("The hypotenuse of Triangle 2 is :"+h.Hypo(5.0,12.0));
		
		System.out.println("The hypotenuse of Triangle 3 is :"+h.Hypo(8.0,15.0));
	}

}
