package Chapter6.Tasks.Task15;

public class HypotenuseCalculations {
	
	public double Hypo(double s1,double s2)
	{
		double sum = (s1 * s1) + (s2 * s2);
		double hypotenuse = Math.sqrt(sum);
		return hypotenuse;
	}


}
